from django.db import models


class Client(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    rut = models.CharField(db_column='RUT', unique=True, max_length=18)  # Field name made lowercase.
    names = models.CharField(db_column='NAMES', max_length=350, blank=True, null=True)  # Field name made lowercase.
    last_name = models.CharField(db_column='LAST_NAME', max_length=150, blank=True, null=True)  # Field name made lowercase.
    second_last_name = models.CharField(db_column='SECOND_LAST_NAME', max_length=150, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=80, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='PHONE', max_length=15, blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return 'ID: {0} | RUT: {1} -> NOMBRE: {2} {3} {4}'.format(self.id, self.rut, self.names, self.last_name, self.second_last_name)

    class Meta:
        managed = False
        db_table = 'client'

    @staticmethod
    def json_deserializer(serializer):
        return Client(
                    id=serializer.data.get('id'),
                    rut=serializer.data.get('rut'),
                    names=serializer.data.get('names'),
                    last_name=serializer.data.get('last_name'),
                    second_last_name=serializer.data.get('second_last_name'),
                    email=serializer.data.get('email'),
                    phone=serializer.data.get('phone'))          


class ContactInformation(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    subject = models.CharField(db_column='SUBJECT', max_length=800, blank=True, null=True)  # Field name made lowercase.
    register = models.DateTimeField(db_column='REGISTER', blank=True, null=True)  # Field name made lowercase.
    attached = models.CharField(db_column='ATTACHED', max_length=1024, blank=True, null=True)
    client = models.ForeignKey(Client, models.DO_NOTHING, db_column='CLIENT_ID', blank=True, null=True)  # Field name made lowercase.

    @staticmethod
    def json_deserializer(serializer):
        attached='http://localhost:8001/media/{0}'.format(serializer.data.get('attached'))
        print(attached)
        return ContactInformation(
                    id=serializer.data.get('id'),
                    subject=serializer.data.get('subject'),
                    register=serializer.data.get('register'),
                    attached=attached,
                    client=Client(id=serializer.data.get('client_id'))) 



    def __str__(self):
        return 'CONTACT: {0}  | {1} | {2} | {3} | {4} |'.format(self.id, self.subject, self.register, self.attached, self.client)

    class Meta:
        managed = False
        db_table = 'contact_information'