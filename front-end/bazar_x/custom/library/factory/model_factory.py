from bazar_x.custom.library.validate.empty import is_emptys
from persistence.models import Client, ContactInformation
from datetime import datetime

def post_parameter(request, name):

    value = ''
    try:
        value = request.POST[name]
        print('{0}: {1}'.format(name, value))
    except Exception as e:
        print(e)

    return value

def get_parameter(request, name):

    value = ''
    try:
        value = request.GET[name]
        print('{0}: {1}'.format(name, value))
    except Exception as e:
        print(e)

    return value

def create_client(request):
    print('create_client')
    rut = post_parameter(request, 'rut')
    nombres = post_parameter(request, 'nombres')
    apellido_paterno = post_parameter(request, 'apellido-paterno')
    apellido_materno = post_parameter(request, 'apellido-materno')
    email = post_parameter(request, 'email')
    telefono = post_parameter(request, 'telefono')
    
    message_error = is_emptys(
        rut=rut,
        nombres=nombres,
        apellido_paterno=apellido_paterno,
        apellido_materno=apellido_materno,
        email=email,
        telefono=telefono
    )

    print('message_error: {0}'.format(message_error))

    if message_error == '':
        return Client(
            rut=rut,
            names=nombres,
            last_name=apellido_paterno,
            second_last_name=apellido_materno,
            email=email,
            phone=telefono
        )
    else:
        raise Exception(message_error)

def create_contact_information(request):
    print('create_contact_information')
    subject = post_parameter(request, 'asunto')
    register = datetime.now()

    message_error = is_emptys(subjec=subject)
    print('message_error: {0}'.format(message_error))

    if message_error == '':
        return ContactInformation(
            subject=subject,
            register=register
        )
    else:
        raise Exception(message_error)

def create_client_id(request):
    print('create_client_id')
    id = get_parameter(request, 'id')
    message_error = is_emptys(id=id)
    if message_error == '' :
        return id
    else:
         raise Exception(message_error)
