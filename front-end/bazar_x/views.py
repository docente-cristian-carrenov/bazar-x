from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from persistence.dao.contact_register import clients_all, contact_information_all, find_client_by_id, delete_client_and_contact, save_client
from datetime import datetime
from django.db import IntegrityError
from django.db.models import F
from bazar_x.custom.library.factory.model_factory import create_client, create_contact_information, create_client_id

def home(request):
    return render(request, "pages/index.html")

def gallery(request):
    return render(request, "pages/gallery.html")

def contact(request):
    return render(request, "pages/contact.html", {'msg_success_contact' : '', 'msg_error_contact' : ''})

def gallery_card(request):
    return render(request, "pages/gallery-card.html")

@csrf_protect
def save_contact_info(request):
    
    print('save_contact_info')
    msg_success_contact = ''
    msg_error_contact = ''

    try:
        client = create_client(request)
        msg_success_contact = save_client(client, request)
        #contact_information = create_contact_information(request)
        #contact_information.attached = request.FILES.get("adjunto")
        #msg_success_contact = save_client_and_contact_info(client, contact_information)
    except Exception as e:
        print('Error:\n{0}'.format(e))
        msg_error_contact = e
    return render(request, 'pages/contact.html',{'msg_success_contact' : msg_success_contact, 'msg_error_contact' : msg_error_contact})

def subjects(request):
    return render(request, "pages/subjects.html", {'clients' : clients_all(), 'contact_informations' : contact_information_all(), 'message_delete_error': ''})        

def delete_client_and_contact_information(request):
    id = create_client_id(request)
    print('id {0}'.format(id))
    message_delete_error = delete_client_and_contact(id)
    return render(request, 'pages/subjects.html', {'clients' : clients_all(), 'contact_informations' : contact_information_all(), 'message_delete_error': message_delete_error})