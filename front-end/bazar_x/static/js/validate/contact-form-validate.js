function enviar() {

    //Obtención de la información del formulario
    let nombres = document.getElementById("txt-nombres").value;
    let apellidoPaterno = document.getElementById("txt-apellido-paterno").value;
    let apellidoMaterno = document.getElementById("txt-apellido-materno").value;
    let email = document.getElementById("txt-email").value;
    let telefono = document.getElementById("txt-telefono").value;
    let asunto = document.getElementById("txt-asunto").value;
    let message = isEmpty(nombres, "NOMBRES");

    //Validación de campos del formulario
    message = message + isMaxLenght(nombres, "NOMBRES", 56);
    message = message + isEmpty(apellidoPaterno, "APELLIDO PATERNO");
    message = message + isMaxLenght(apellidoPaterno, "APELLIDO PATERNO", 81);
    message = message + isEmpty(apellidoMaterno, "APELLIDO MATERNO");
    message = message + isMaxLenght(apellidoMaterno, "APELLIDO MATERNO", 81);
    message = message + isEmpty(email, "EMAIL");
    message = message + isMaxLenght(email, "EMAIL", 71);
    message = message + isEmail(email, "EMAIL");
    message = message + isEmpty(telefono, "TELEFONO");
    message = message + isMaxLenght(telefono, "TELEFONO", 12);
    message = message + isEmpty(asunto, "ASUNTO");
    message = message + isMaxLenght(asunto, "ASUNTO", 501);

    //Mostrar Error
    if (message.length > 0) {

        crearMensajeErrorYExito("danger", message);
    } else {
        //Mostrar mensaje de exito
        document.getElementById("frm-contact").submit();
    }
}

function crearMensajeErrorYExito(type, message) {

    console.log(message);
    let divMessage = document.getElementById("message-error");
    let newMessage = "<div class=\"alert alert-" + type + " alert-dismissible fade show\" role=\"alert\">" +
        "<div id=\"message\"> " + replaceAll(message, "\n", "<br>") + " </div>" +
        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
        "<span aria-hidden=\"true\">&times;</span></button></div>";
    divMessage.innerHTML = newMessage;
}