#INSTALL DJANGO
python -m pip install Django

#CREACIÓN PROYECTO WEB
django-admin startproject

#CREACIÓN PROYECTO DE PERSISTENCIA
python.exe .\manage.py startapp persistence

#COMANDO PARA CREAR MODELO A PARTIR DE ENTIDADES
python.exe .\manage.py inspectdb

#COMANDO PARA FORMATO TRANSACCIONAL
pip install pylint-django

#CONFIGURACIÓN DE PLUGIN DE FORMATO TRANSACCIONAL
#PRIMER PASO EJECUTAR COMANDO
CTRL + SHIFT + P -> BUSCAR >prel
#SEGUNDO PASO SELECCIONAR LENGUAJE PYTHON
SELECCIONAR CONFIGURACIÓN DE LENGUAJE, LUEGO BUSCAR PYTHON
#EDITAR ARCHIVO
SOBREESCRIBIR ARCHIVO settings.json CON 

{
    "python.linting.pylintArgs": [
        "--load-plugins=pylint_django"
    ],

    "[python]": {

    }
}

#OPCIONAL SOLO SI CREAN MODELO Y LUEGO QUIEREN MIGRAR A LA BASE DE DATOS
#COMANDO PARA CREAR MIGRACIÓN MODELO A ENTIDADES
python.exe .\manage.py makemigrations persistence
#COMANDO PARA EJECUTAR MIGRACIÓN DE MODELO A ENTIDADES
python.exe .\manage.py migrate persistence


# INSTALL PILLOW
python -m pip install Pillow


#OPCIONAL SOLO SI CREAN MODELO Y LUEGO QUIEREN MIGRAR A LA BASE DE DATOS
#COMANDO PARA CREAR MIGRACIÓN MODELO A ENTIDADES
python.exe .\manage.py makemigrations persistence
python.exe .\manage.py migrate
python.exe .\manage.py createsuperuser
# Username (leave blank to use 'cristiancarreno'): admin 
# Email address: cristian_c_v@hotmail.com
# Password: Dj4nG0.2020

