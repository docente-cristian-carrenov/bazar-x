from rest_framework import serializers


class ClientSerializers(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    rut = serializers.CharField(required=True, max_length=18)
    names = serializers.CharField(required=True, max_length=350)
    last_name = serializers.CharField(required=True, max_length=150)
    second_last_name = serializers.CharField(required=True, max_length=150)
    email = serializers.CharField(required=True, max_length=80)
    phone = serializers.CharField(required=True, max_length=15)

class IdSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)

