from django.db import models


class Client(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    rut = models.CharField(db_column='RUT', unique=True, max_length=18)  # Field name made lowercase.
    names = models.CharField(db_column='NAMES', max_length=350, blank=True, null=True)  # Field name made lowercase.
    last_name = models.CharField(db_column='LAST_NAME', max_length=150, blank=True, null=True)  # Field name made lowercase.
    second_last_name = models.CharField(db_column='SECOND_LAST_NAME', max_length=150, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=80, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='PHONE', max_length=15, blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return 'ID:{0} RUT: {1} -> NOMBRE: {2} {3} {4} -> EMAIL: {5} PHONE: {6}'.format(self.id, self.rut, self.names, self.last_name, self.second_last_name, self.email, self.phone)

    class Meta:
        managed = False
        db_table = 'client'


    def json_serializer(self):
        return {
                'id': self.id,
                'rut': self.rut,
                'names': self.names,
                'last_name': self.last_name,
                'second_last_name': self.second_last_name,
                'email': self.email,
                'phone': self.phone}
    

    @staticmethod
    def json_deserializer(serializer):
        return Client(
                    id=serializer.data.get('id'),
                    rut=serializer.data.get('rut'),
                    names=serializer.data.get('names'),
                    last_name=serializer.data.get('last_name'),
                    second_last_name=serializer.data.get('second_last_name'),
                    email=serializer.data.get('email'),
                    phone=serializer.data.get('phone'))         

class ContactInformation(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    subject = models.CharField(db_column='SUBJECT', max_length=800, blank=True, null=True)  # Field name made lowercase.
    register = models.DateTimeField(db_column='REGISTER', blank=True, null=True)  # Field name made lowercase.
    attached = models.ImageField(db_column='UPLOAD_IMAGE', upload_to='contacts')
    client = models.ForeignKey(Client, models.DO_NOTHING, db_column='CLIENT_ID', blank=True, null=True)  # Field name made lowercase.

    @property
    def image_url(self):
        from django.contrib.sites.models import Site
        domain = Site.objects.get_current().domain
        url = 'http://{domain}'.format(domain=domain)

        if self.attached and hasattr(self.attached, 'url'):
            return url + self.attached.url

    def __str__(self):
        return 'CONTACT: {0} - REGISTER: {1} | {2}'.format(self.subject, self.register, self.client)

    class Meta:
        managed = False
        db_table = 'contact_information'
